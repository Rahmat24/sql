1. Membuat Database
CREATE DATABASE myshop;

2. Membuat Table di Dalam Database

CREATE TABLE users(
    	id INT(8) PRIMARY KEY AUTO_INCREMENT,
    	name varchar(255),
    	email varchar(255),
    	password varchar(255),
);


CREATE TABLE categories(
    	id INT(8) PRIMARY KEY AUTO_INCREMENT,
    	name varchar(255),
);

CREATE TABLE items(
	id INT(8) PRIMARY KEY AUTO_INCREMENT,
	name varchar(255),
	description varchar(255),
	price int (30),
	stock int(30),
	category_id int(8)
    	FOREIGN KEY (category_id) REFERENCES categories(id)
);


3. Memasukkan Data pada Table
INSERT INTO users ( name, email, password) VALUES ("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123") ;

INSERT INTO categories ( name) VALUES ("gadget"), ("cloth"), ("men"), ("women"), ("branded") ;

INSERT INTO items (name, description, price, stock, category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO watch", "jam tangan anak yang jujur banget", 20000000, 10, 1);

4. Mengambil Data dari Database
	- Mengambil data users
	SELECT id, name, email FROM users;
	- Mengambil data items
	SELECT * FROM items WHERE price > 1000000;
	SELECT * FROM items WHERE name LIKE 'Uniklo%';
	- Menampilkan data items join dengan kategori
	SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori SUM(orders.amount) as total_amount
	FROM items
	INNER JOIN categories ON categories.id = items.category_id;
	
5. Mengubah Data dari Database
	UPDATE items
	SET price = 2500000 
	WHERE name = "sumsang b50";